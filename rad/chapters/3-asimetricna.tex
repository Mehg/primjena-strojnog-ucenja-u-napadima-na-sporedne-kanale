\chapter{Asimetrična kriptografija}

Asimetrična kriptografija, također poznata kao kriptografija javnog ključa, je kriptografska metoda koja koristi par matematički povezanih ključeva: javni ključ i privatni ključ. Javni ključ se koristi za enkripciju, omogućujući svakome da šifrira poruke namijenjene vlasniku odgovarajućeg privatnog ključa. Privatni ključ drži se u tajnosti i koristi se za dešifriranje. Ova asimetrija u korištenju ključa osigurava sigurne komunikacijske kanale bez potrebe za prethodnom razmjenom ključeva. Međutim, učinkovitost asimetrične kriptografije oslanja se na računsku težinu određenih matematičkih problema, kao što je rastavljanje velikih brojeva na faktore ili izračunavanje diskretnih logaritama. Najpoznatiji algoritam asimetrične kriptografije je RSA (Rivest–Shamir–Adleman) čije smo iščitavanje tajnog ključa vidjeli na slici \ref{rsa}.


Prednost asimetrične kriptografije je da ne treba postojati prethodna razmjena ključeva između strana za ostvarenje komunikacije, no to svojstvo sa sobom donosi i mane. Glavna mana je da ne postoji dokaz da se strana B nije lažno predstavila. Potrebna je provjera autentičnosti - sigurnosno svojstvo koje osigurava strani A da je strana B doista poslala poruku M i da je ta poruka izvorna i nepromijenjena.  Autentifikaciju poruke moguće je izvršiti algoritmima digitalnog potpisa (engl. \textit{Digital Signature Algorithm}) prikazanog na slici \ref{dsa}. Zbog veće sigurnosti i brzine, sve prisutnija je kriptografija temeljena na eliptičkim krivuljama (engl. \textit{elliptic curve cryptography} - ECC).


\begin{figure}[H]
	\includegraphics[scale=0.53]{images/dsa.png} 
	\centering
	\caption{Algoritam digitalnog potpisa}
	\label{dsa}
\end{figure}

Kriptografija eliptičke krivulje je moderna kriptografska tehnika koja se temelji na matematici eliptičkih krivulja nad konačnim poljima. Nudi vrlo učinkovitu i sigurnu metodu za razmjenu ključeva, digitalne potpise i enkripciju, a oslanja se na težinu izračuna diskretnog logaritma eliptičke krivulje,  koji kaže da je jednostavno i stoga učinkovito izračunati $Q = k \cdot P$, ali je teško pronaći $k$ znajući $Q$ i $P$. U usporedbi s tradicionalnim kriptografskim sustavima, nudi istu razinu sigurnosti s kraćim duljinama ključeva, što ga čini računalno učinkovitim i prikladnim za okruženja s ograničenim resursima kao što su mobilni uređaji. Danas, kriptografija eliptičke krivulje koristi se u mnogim područjima poput komunikacijskih protokola kao što su Transport Layer Security (TLS) i Secure Shell (SSH), digitalnim potpisima, kriptovalutama i za osiguravanje sigurnosti mobilnih uređaja. Jedan od poznatijih algoritama digitalnog potpisa u kriptografiji eliptičkih krivulja je i EdDSA.


\section{EdDSA}

EdDSA (Edwards-curve Digital Signature Algorithm) je varijanta Schnorrove sheme digitalnog potpisa (algoritama digitalnog potpisa temeljenih na eliptičkim krivuljama) koja koristi zakrivljene Edwardove krivulje (engl. \textit{Twisted Edward Curves}), podskupinu eliptičkih krivulja koje omogućuju ubrzanja za specifične parametre krivulje i koje su razlog zašto EdDSA koja nudi visoke performanse uz izbjegavanje sigurnosnih problema koji su se pojavili u drugim shemama digitalnog potpisa. Algoritam EdDSA opisan je u radu \cite{bernstein_2012_highspeed}. Iskrivljena Edwardova krivulja $E_{E_{a,d}}$ preko polja $\mathbb{K}$ je afina ravninska krivulja, krivulja kojoj su sve točke mu jednoj ravnini, definirana formulom \ref{tec}.

\begin{equation}
    \begin{array}{l}
         E_{E_{a,d}}: ax^2 + y^2 = 1 + dx^2y^2 \\
          a, d \neq 0 \in \mathbb{K}
    \end{array}   
    \label{tec}
\end{equation}


EdDSA, poput mnogih algoritama potpisivanja, koristi jednokratni ključ (engl. \textit{ephemeral key}), različit za svaku poruku, ali bez korištenja generatora slučajnih brojeva kako bi se spriječili napadi kod korištenja predvidljivog generatora slučajnih brojeva. Algoritam generiranja ključeva, potpisivanja i verificiranja potpisa opisan je pseudokodom \ref{edDSA_pseudo}, a korištena notacija dana je u tablici \ref{edDSA_notation}. Jednokratni privatni ključ $r$ je sažetak konkatenirane poruke $M$ i pomoćnog ključa $b$, te se iz njega gradi jedinstveni jednokratni javni ključ $R$. $R$ dalje koristimo, uz javni ključ $P$ i poruku $M$, u izračunavanju sažetka pomoću kojeg se na kraju, uz jednokratni privatni ključ $r$ i privatni skalar $a$ računa $S$, koji uz jednokratni javni ključ $R$ čini digitalni potpis. Nakon generiranja potpisa, strana A šalje strani B trojku $(M, R, S)$ pomoću koje strana B verificira potpis. Ako se algoritam verifikacije uspješno izvede, poruka koju je primila strana B je autentična poruci koju je poslala strana A. Validnost algoritma validacije dokazana je u \ref{edDSA_validity}. Algoritam koristi i neke javno poznate parametre, poput $B$ i $l$,  specifične za korištenu krivulju. Detaljniji opis algoritama dan je u \cite{edDSA}. Posebna vrsta EdDSA algoritma koja koristi krivulju nazvanu \textit{Curve25519} i SHA-512 algoritam računanja sažetka nazivamo Ed25519. Značajne upotrebe Ed25519 uključuju OpenSSH, GnuPG te signify alat OpenBSD.



\begin{table}[H]
    \centering
        \begin{tabular}{cc}
             & simbol \\ 
            \hline
            privatni ključ & $k$\\
            privatni skalar & $a$  \\
            pomoćni ključ & $b$ \\
            jednokratni privatni ključ & $r$  \\
            poruka & $M$\\
            \hline
        \end{tabular}
        \caption{EdDSA Notacija}
    \label{edDSA_notation}
\end{table}


\begin{algorithm}
  \caption{EdDSA generiranje ključeva, potpisivanje i verifikacija}

  \SetKwFunction{keyGen}{}
  \SetKwProg{Fn}{Generiranje parova ključeva}{:}{}
  \Fn{\keyGen{$k$}}{
    Hash $k$ tako da $H(k) = (h_0, h_1, \dots , h_n) = (a,b)$\\
    $a = (h_0, \dots, h_{\frac{n}{2}})$\\
    $b = (h_{\frac{n}{2}+1}, \dots, h_n)$\\
    Izračunaj javni ključ: $P = aB$
  }

  \SetKwProg{Fn}{Potpisivanje}{:}{}
  \Fn{\keyGen{$M$}}{
    Izračunaj jednokratni privatni ključ $r = H(b, M)$ \\
    Izračunaj jednokratni javni ključ $R = rB$ \\
    $h = H(R, P, M)$ mod $l$ \\
    $S = (r + ha)$ mode $l$ \\
    Pošalji potpis $(R, S)$ uz poruku $M$
  }

  \SetKwProg{Fn}{Verifikacija}{:}{}
  \Fn{\keyGen{$M$, $R$, $S$}}{
    $h = H(R, P, M)$\\
    Provjeri je li $8SB = 8R + 8hP$ u $E$
  }
  
  \label{edDSA_pseudo}
\end{algorithm}

\begin{algorithm}
    \caption{EdDSA točnost validacije}
    $8SB = 8R + 8hP$\\
    $8 \cdot (r + a \cdot H(R, P, M)) \cdot B = 8 \cdot (rB) + 8 \cdot H(R, P, M) \cdot aB$\\
    $8B \cdot (r + a\cdot H(R, P, M)) = 8B \cdot (r + a \cdot H(R, P, M))$\\
    \checkmark
  \label{edDSA_validity}
\end{algorithm}

\eject


Sigurnost ECC algoritama ovisi o sposobnosti izračunavanja umnoška točaka i pretpostavljenoj nemogućnosti obrnutog izračuna da bi se dohvatio množenik s obzirom na izvorne točke i umnožak. Konkretno, cilj je izdvojiti jednokratni privatni ključ $r$ iz njegovog skalarnog množenja s baznom točkom $B$ eliptičke krivulje čime se dobiva jednokratni javni ključ $R$. Wolf SSL-ova implementacija množenja $r$ i $B$ u algoritmu Ed25519 koristi se pomičnim okvirima s unaprijed izračunatom tablicom svih mogućih rezultata množenja dijelova $r$ i $B$. Korištenje prethodno izračunate tablice korisno je, ne samo zbog ubrzanja računanja (kompromis između vremena i memorije), već i zbog konstantnog vremena računanja čime postaje sigurna na napade mjerenjem vremena. Curenje informacija temelji se na učitavanju određenih vrijednosti u memoriju pomoću kojih možemo povezati koji ključ $r$ se koristi. 


Općenito u kriptografiji eliptičkih krivulja preporučeno je korištenje različitih jednokratnih ključeva. Posljedica korištenja istih jednokratnih ključeva je trivijalno izračunavanje privatnog skalara $a$ iz dva para potpisa $(R, S)$ i $(R, S')$ za poruke $M$ i $M'$ čiji je postupak prikazan u jednadžbi \ref{rev_r}. 


\begin{equation}
    \begin{array}{l}
         (M, R, S), (M', R, S') \\
         z \leftarrow H(M)\\
         z' \leftarrow H(M')\\
         r = (z - z')(S - S')^{- 1} \\
         a = R^{-1}(Sr - z)
    \end{array}   
    \label{rev_r}
\end{equation}


Kao i kod drugih algoritama digitalnog potpisa, EdDSA koristi tajnu jednokratnu vrijednost (engl. \textit{nonce}) jedinstvenu za svaki potpis. U algoritmima poput DSA i ECDSA, ta vrijednost se tradicionalno generira nasumično za svaki potpis — i ako se generator slučajnih brojeva ikada može predvidjeti kada se potpisuje, iz potpisa može procuriti privatni ključ. Za razliku od tradicionalnih algoritama potpisivanja, EdDSA deterministički odabire nasumičnu vrijednost kao sažetak dijela privatnog ključa i poruke. Zbog toga se dva ista jednokratna ključa $r$ ne mogu dogoditi tj. napad opisan u \ref{rev_r} nije moguć. Međutim, tajni skalar $a$ moguće je dobiti na dva načina: napad na implementaciju funkcije sažetka za oporavak $b$ iz jednokratnog privatnog ključa $r$ ili napad na implementaciju množenja tijekom izračuna jednokratnog privatnog ključa $r$ što uključuje prikupljanje mjerenja (engl. \textit{traces}) iz sporednih kanala. Napad na implementaciju funkcije sažetka za oporavak $b$ iz jednokratnog privatnog ključa $r$ opisan je u radu \cite{eddsahashbreak}.


\subsection{Skup podataka}

Skup podataka, opisan u radu \cite{oneTrace}, sakupljen je osciloskopskim snimanjem potrošnje električne energije na razvojnoj ploči Pi\~{n}ata koja se temelji na 32-bitnom mikrokontroleru STM32F4 s arhitekturom temeljenom na ARM-u, koja radi na taktnoj frekvenciji od 168 MHz. Napada se Ed25519 implementacija Wolf SSL-a 3.10.2. Profilira se skalarno množenje eliptičke krivulje privatnog jednokratnog ključa $r$ s baznom točkom krivulje $B$, točnije, fokusira se na operaciju dohvaćanja iz tablice (engl. \textit{Lookup Table (LUT)}) koja se koristi za dohvaćanje unaprijed izračunatih dijelova rezultata iz tablice koja je pohranjena u memoriji. Zbog efikasnosti, 256-bitni jednokratni privatni ključ $r$ tumači se u odsječcima od $4$ bita (engl. \textit{nibble}) $e[i], i \in [0, 63]$, a za izračunavanje $R = rB$, potrebno je izračunati $\sum_{i=0}^{63} e[i]16^iB$. Kako množenje zahtijeva resurse, implementacija pohranjuje rezultate svih mogućih množenja odsječaka u unaprijed izračunatu tablicu i učitava odgovarajuće dijelove po potrebi.


Skup podataka, dostupan u repozitoriju \cite{eddsa_dataset}, sastavljen je od dva dijela: mjerenja za profiliranje (engl. \textit{profiling traces}) i mjerenja za napad (engl. \textit{attack traces}). Svaki od dva dijela sastoji se od dvije grupe: mjerenja (engl. \textit{traces}) i labele (engl. \textit{lables}). Grupa "mjerenja" sastoji se od prikupljenih mjerenja pri enkripciji s različitim odsječcima ključa. Jedno mjerenje se sastoji od $1000$ uzoraka koji predstavljaju relevantne informacije iz jednog koraka enkripcije. Grupa "labele" sastoji se od točnih $4$-bitnih odsječaka ključeva za odgovarajuće mjerenje. Ukupno ima $16$ mogućih klasa tj. svi mogući isječci ključeva. Cijeli skup podataka ima $6400$ označenih mjerenja od kojih je $5120$ u mjerenjima za profiliranje i $1280$ u mjerenjima za napad. U sklopu ovog rada mjerenja za profiliranje podijeljeni su u dva dijela u omjeru $80:20$ na skup za učenje, koji se sastoji od $4096$ mjerenja, i skup za validaciju, koji se sastoji od $1024$ mjerenja. Vizualna podjela skupa podataka prikazana je slikom \ref{eddsa_data}.



\begin{figure}[H]
	\includegraphics[scale=1]{images/eddsa/eddsa.png} 
	\centering
	\caption{Podjela skupa podataka}
	\label{eddsa_data}
\end{figure}



\subsection{Modeli}

Istrenirano je i vrednovano ukupno $4$ različita modela: model konvolucijske neuronske mreže (CNN), model potpuno povezane neuronske mreže (MLP), SVM model s linearnom i RBF jezgrom te LSTM model. Hiperparametri za sve modele prikazani su tablicom \ref{eddsa_hyp}. Kao algoritam optimizacije korišten Adam optimizator s funkcijom gubitka unakrsne entropije \ref{cee_loss}.


\begin{table}[H]
    \centering
        \begin{tabular}{cccc}
             & CNN, MLP & SVM & LSTM \\ 
            \hline
            veličina mini grupe & 32 & 32 & 32\\
            broj epoha & 10 & - & 20  \\
            stopa učenja & 0.001 & - & 0.001 \\
            \hline
        \end{tabular}
        \caption{Hiperparametri}
    \label{eddsa_hyp}
\end{table}


\begin{equation}
        E(W | D) = - \sum_{i=1}^N \sum_{k=1}^K y_k^i \cdot ln h_k(x^i;W)
    \label{cee_loss}
\end{equation}

Model konvolucijske neuronske mreže sastoji se od $3$ konvolucijska sloja praćena jednim potpuno povezanim slojem. Konvolucijski sloj sastoji se od operacije konvolucije, aktivacijske funkcije ReLU praćene u svakom drugom sloju normalizacijom nad grupom (engl. \textit{batch norm}) te sažimanja maksimumom. Nakon konvolucijskih slojeva slijedi Dropout sloj te potpuno povezan sloj s aktivacijskom funkcijom softmax . Veličina jezgre u operaciji konvolucije i sažimanju je $3$, s korakom $1$ u operaciji konvolucije i korakom $3$ u operaciji sažimanja. Arhitektura je također prikazana slikom \ref{eddsa_cnn_arh} u kojoj jedan blok predstavlja obavljenu jednu operaciju konvolucije, aktivacija ReLU, ako ispod bloka piše BN onda i obavljenu normalizaciju nad grupom, te sažimanje maksimumom. Brojka iznad bloka predstavlja dimenzionalnost podataka, a Dropout označava primjenu tehnike ispadanja. Kvadrat s krugovima predstavlja potpuno povezan sloj.


\begin{figure}[H]
	\includegraphics[scale=0.65]{images/eddsa/cnn.png} 
	\centering
	\caption{Arhitektura konvolucijske neuronske mreže}
	\label{eddsa_cnn_arh}
\end{figure}


Model potpuno povezane neuronske mreže sastoji se od $2$ potpuno povezana sloja s aktivacijskom funkcijom ReLU. U drugom sloju se primjenjuje tehnika ispadanja te na kraju drugog sloja koristi se aktivacijska funkcija softmax. LSTM model sastoji se od jedne dvoslojne LSTM ćelije te jednim potpuno povezanim slojem praćenim softmax aktivacijskom funkcijom.


\subsection{Rezultati}

Metrike razmatrane nad ovim modelima uključuju gubitak na skupu za učenje te validacijskom skupu podataka, točnost validacijskom skupu podataka te točnost na skupu za testiranje. Slike \ref{eddsa_loss} prikazuju gubitak svih modela na skupu za treniranje te gubitak na validacijskom skupu. Gubitak konvolucijske mreže u oba slučaja je mnogo niži od svih drugih modela, izuzev LSTM modela koji postiže slične iznose za mnogo veći broj epoha. Konvolucijski model postiže zadovoljavajuće iznose već u prvih par epoha što nas upućuje na to da postoje nekakve lokalne ovisnosti u mjerenjima. Na slici \ref{eddsa_valid_acc} prikazana je točnost koju svi modeli postižu nakon svake epohe. Iz ovog grafa vidljivo je da su svi modeli zapravo zadovoljavajući i da postižu visoku točnost u samo par epoha. Također se vidi da konvolucijske mreže postižu savršenu točnost odmah nakon prve epohe. Do sada u slike nije bio uključen SVM zbog specifičnog načina treniranja. 


Nadalje, ovi rezultati prikazuju točnost kod oporavka $4$-bitnog odsječka ključa, te kako bismo imali predodžbu o tome koliko su te metode dobre za oporavak punog $256$-bitnog ključa, moramo primijeniti klasifikaciju na $64$ uzastopna $4$-bitna odsječka ključa. Rezultantnu vjerojatnost oporavka cijelog ključa $p$ možemo opisati kao umnožak vjerojatnosti oporavka svakog od pojedinih odsječaka $p_o$: $p = \prod_{i=0}^{64} p_{o,i}$. Tablica \ref{eddsa_test_acc} prikazuje rezultate točnosti na skupu za testiranje za sve modele, kao i vjerojatnost oporavka cijelog ključa. Rezultati pokazuju da svi modeli dostižu savršenu ili gotovo savršenu točnost na skupu za testiranje, tj. vjerojatnost da se oporavi jedan $4$-bitni odsječak ključa uvijek je viša od $90\%$. Međutim, potenciranje s eksponentom $64$ jako smanjuje vjerojatnost oporavka cijelog ključa pa je tako vjerojatnost oporavka cijelog ključa za MLP model $56\%$. Iako je to znatno manje od drugih modela, u kontekstu napada na sporedne kanale EdDSA algoritma to bi značilo da je u prosjeku potrebno samo $2$ pokušaja. 



\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/eddsa/train_loss.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_loss.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu}%
    \label{eddsa_loss}%
\end{figure}


\begin{figure}[H]
	\includegraphics[scale=0.65]{images/eddsa/valid_acc.png} 
	\centering
	\caption{Točnost na validacijskom skupu}
	\label{eddsa_valid_acc}
\end{figure}


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccccc}
             točnost & CNN & MLP & SVM (linear) & SVM (RBF) & LSTM \\ 
            \hline
            $4$-bitni odsječak & 1.00 & 0.990 & 0.998 & 0.998 & 0.998 \\
            cijeli ključ & 1.0 & 0.526 & 0.880 & 0.880 & 0.880 \\
            \hline
        \end{tabular}
        \caption{Točnost na skupu za testiranje}
    \label{eddsa_test_acc}
\end{table}


Jedna mjera u kriptografiji koja se koristi za sprječavanje napada na sporedne kanale je dodavanje nasumičnog šuma. Ideja je da se prikriju procurjele informacije beznačajnim šumom te se tako oteža analiza mjerenja. Isti efekt moguće je simulirati dodavanjem Gaussovog šuma na mjerenja u skupu podataka.

\section{Dodavanje šuma}

Analizom skupa podataka ispostavilo se da su minimalna i maksimalna vrijednost u mjerenjima u skupu podataka za profiliranje jednaki $-0.3$ i $0.3$, a u skupu za napad $-0.1$ i $0.1$. Uzimajući te vrijednosti, na mjerenja u skupu podataka dodan je Gaussov šum sa $\mu = 0$ i $\sigma = 0.01$. Hiperparametri i parametri svih modela su isti, osim što se za LSTM model smanjio broj epoha na $10$. Slike \ref{eddsa_loss_noise} prikazuje gubitak na skupu podataka za učenje i na skupu podataka za validaciju. Kao i prethodno, prikazana je točnost na validacijskom skupu na slici \ref{eddsa_valid_acc_noise}, te točnosti u tablici \ref{edDSA_test_acc_noise}. Rezultati su, kao i prethodno, iznimno dobri za oporavak $4$-bitnog odsječka. Točnost LSTM modela sa smanjenim epohama ukazuje na to da šum čak pomaže pri učenju modela i generalizaciji. Vjerojatnost oporavak cijelog ključa značajno se smanjila za MLP i LSTM modele, ali je i dalje $100\%$ za CNN model. 




\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/eddsa/train_loss_noise.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_loss_noise.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu s dodatnim šumom}%
    \label{eddsa_loss_noise}%
\end{figure}


\begin{figure}[H]
	\includegraphics[scale=0.65]{images/eddsa/valid_acc_noise.png} 
	\centering
	\caption{Točnost na validacijskom skupu s dodatnim šumom}
	\label{eddsa_valid_acc_noise}
\end{figure}


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccccc}
            točnost & CNN & MLP & SVM (linear) & SVM (RBF) & LSTM \\ 
            \hline
            $4$-bitni odsječak & 1.0 & 0.983 & 0.997 & 0.995 & 0.992\\
            cijeli ključ & 1.0 & 0.334 & 0.825 & 0.726 & 0.598\\
            \hline
        \end{tabular}
        \caption{Točnost na skupu za testiranje s dodatnim šumom}
    \label{edDSA_test_acc_noise}
\end{table}


Nakon što su modeli ostvarili toliko dobre rezultate, bilo bi interesantno pogledati koji je minimalni broj potrebnih mjerenja dovoljan da model pronađe odgovarajući ključ te kako se modeli ponašaju sa smanjenim brojem mjerenja. 


\section{Smanjen broj mjerenja}


Treniranje modela izvršeno je na dva smanjena skupa podataka: prvi sadrži upola manje mjerenja od originalnog skupa podataka, a drugi sadrži samo $20\%$ originalnog skupa podataka. U prvom slučaju, skup podataka za učenje sadrži $2560$ mjerenja, a u drugom $1024$. Primjeri koji nisu korišteni u skupu za učenje prebačeni su u skup za validaciju. Hiperparametri te parametri svih modela postavljeni su na iste vrijednosti kao do sada. Kao i u prethodnim slučajevima, iz slika \ref{eddsa_loss_50}, \ref{eddsa_valid_acc_50} te iz tablice \ref{edDSA_test_acc_50} vidljivo je da svi modeli postižu visoku točnost kad su učeni na skupu podataka s upola manje mjerenja, te možemo vidjeti blaga smanjenja u točnosti svih modela osim CNN modela. Najveće promjene vidimo kod vjerojatnosti oporavka cijelog ključa: vjerojatnost MLP model se smanjila za $17\%$, LSTM modela za $14\%$, a SVM linearnog modela i RBF modela za $24\%$ i $39\%$. Iz ovih rezultata možemo vidjeti da je smanjenje broja mjerenja imao najveći utjecaj na SVM model. 

\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/eddsa/train_loss_50.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_loss_50.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu kod treniranja s upola manjim skupom za učenje}%
    \label{eddsa_loss_50}%
\end{figure}


\begin{figure}[H]
	\includegraphics[scale=0.65]{images/eddsa/valid_acc_50.png} 
	\centering
	\caption{Točnost na validacijskom skupu kod treniranja s upola manjim skupom za učenje}
	\label{eddsa_valid_acc_50}
\end{figure}


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccccc}
            točnost & CNN & MLP & SVM (linear) & SVM (rbf) & LSTM \\ 
            \hline
            $4$-bitni odsječak & 1.0 & 0.984 & 0.993 & 0.989 & 0.995\\
            cijeli ključ & 1.0 & 0.356 & 0.638 & 0.493 & 0.736\\
            \hline
        \end{tabular}
        \caption{Točnost na skupu za testiranje kod treniranja s upola manjim skupom za učenje}
    \label{edDSA_test_acc_50}
\end{table}


 Slike \ref{eddsa_loss_20}, \ref{eddsa_valid_acc_20}, te tablica \ref{eddsa_test_acc_20} prikazuju rezultate treniranja na skupu podataka koji se sastoji od samo $20\%$ mjerenja originalnog skupa podataka. Rezultati pokazuju da svi modeli postižu visoku točnost za oporavak $4$-bitnog odsječka ključa uz samo $1024$ mjerenja za treniranje. Svi modeli, osim CNN modela, postižu nešto niže točnosti za oporavak $4$-bitnog odsječka od prethodnog slučaja, a CNN model i dalje postiže savršenu točnost. Vjerojatnost oporavka cijelog ključa se znatno smanjila kod svih modela, te je kod LSTM modela manja od $0.1\%$. Činjenica da CNN model postiže savršenu točnost dovodi nas do pitanja koliki je najmanji potreban broj mjerenja, a da CNN model i dalje postiže vrlo visoku točnost.
 
\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/eddsa/train_loss_20.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_loss_20.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu kod treniranja na skupu za učenje koji se sastoji od $20\%$ mjerenja originalnog skupa podataka}%
    \label{eddsa_loss_20}%
\end{figure}


\begin{figure}[H]
	\includegraphics[scale=0.65]{images/eddsa/valid_acc_20.png} 
	\centering
	\caption{Točnost na validacijskom skupu kod treniranja na skupu za učenje koji se sastoji od $20\%$ mjerenja originalnog skupa podataka}
	\label{eddsa_valid_acc_20}
\end{figure}


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccccc}
            točnost & CNN & MLP & SVM (linear) & SVM (RBF) & LSTM \\ 
            \hline
            $4$- bitni odsječak& 1.0 & 0.964 & 0.986 & 0.980 & 0.892\\
            cijeli ključ & 1.0 & 0.096 & 0.406 & 0.274 & 0.001\\
            \hline
        \end{tabular}
        \caption{Točnost na skupu za testiranje kod treniranja na skupu za učenje koji se sastoji od $20\%$ mjerenja originalnog skupa podataka}
    \label{eddsa_test_acc_20}
\end{table}



\section{Minimalan broj mjerenja}

Za pronalazak minimalnog potrebnog broja mjerenja s kojim CNN model postiže visoku točnost isprobano je par različitih veličina skupa podataka za učenje. Slike \ref{eddsa_loss_cnn_min} prikazuju gubitak na skupu za učenje i na validacijskom skupu za modele trenirane na različitim veličinama skupa za učenje. Slike \ref{eddsa_valid_acc_cnn_min} prikazuju točnost na skupu za validaciju za modele trenirane na različitim veličinama skupa za učenje. Lijeva slika prikazuje više modela treniranih $10$ epoha. Možemo uočiti da modeli s više od $190$ mjerenja postižu točnost veću od $90\%$ na validacijskom skupu unutar $10$ epoha. Modeli sa $70$, $100$ i $150$ mjerenja trenirani su ponovno, ali ovaj put $100$ epoha jer njihovi gubitci i točnost na validacijskom skupu ukazuju da bi mogli s vremenom naučiti bolje generalizirati. Desna slika prikazuje točnosti tih modela na validacijskom skupu te vidimo da model treniran sa $150$ mjerenja uspije postići točnost oko $90\%$. Tablica \ref{eddsa_test_acc_min} sadrži točnosti svih modela na skupu za testiranje, gdje su svi modeli, izuzev modela sa $70$, $100$ i $150$ mjerenja trenirani $10$ epoha, dok su modeli sa $70$, $100$ i $150$ mjerenja trenirani $100$ epoha. Iz tablice možemo iščitati kako je CNN modelu potrebno oko $150$ mjerenja uz $100$ epoha kako bi postigao točnost veću ili jednaku $90\%$ kod oporavka $4$-bitnog odsječka ključa. Isti taj model postiže vjerojatnost oporavka cijelog ključa $0.635\%$ što znači da u prosjeku treba oko $160$ pokušaja kako bi se oporavio cijeli ključ. Također je interesantno da je korišten model konvolucijske neuronske mreže vrlo jednostavan, te bi neki složeniji mogao postići bolje rezultate.

 
\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/eddsa/train_loss_cnn_min.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_loss_cnn_min.png} }}%
    \qquad
    \caption{Gubitak CNN modela na skupu za učenje i validacijskom skupu kod treniranja na skupovima za učenje različitih veličina. Broj predstavlja veličinu skupa za učenje.}%
    \label{eddsa_loss_cnn_min}%
\end{figure}



\begin{figure}[H]%
    \centering
    \subfloat[\centering Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_acc_cnn_min.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/eddsa/valid_acc_cnn_min_100.png} }}%
    \qquad
    \caption{Točnost CNN modela na validacijskom za 10 i 100 epoha kod treniranja na skupovima za učenje različitih veličina}%
    \label{eddsa_valid_acc_cnn_min}%
\end{figure}



\begin{table}[H]
    \centering
        \begin{tabular}{c|ccccc|cc}
            broj mjerenja & $4$-bita & cijeli ključ  &   &   & broj mjerenja & $4$-bita & cijeli ključ\\ 
            \cline{0-2} \cline{6-8} 
             $50$ & $0.578$ & $0.000$ &  &  & $190$ & $0.985$ & $0.380$\\
             $70$ & $0.817$ & $0.000$ &  &  & $220$ & $1.000$ & $1.000$\\
            $100$ & $0.818$ & $0.000$ &  &  & $280$ & $1.000$ & $1.000$\\
            $150$ & $0.924$ & $0.006$ &  &  & $512$ & $1.000$ & $1.000$\\
            \cline{0-2} \cline{6-8} 
        \end{tabular}
        \caption{Točnost CNN modela na skupu za testiranje kod treniranja na skupovima za učenje različitih veličina. Svi modeli, izuzev modela sa $70$, $100$ i $150$ mjerenja trenirani su $10$ epoha, dok su modeli sa $70$, $100$ i $150$ mjerenja trenirani $100$ epoha}
    \label{eddsa_test_acc_min}
\end{table}


