\chapter{Simetrična kriptografija}


Simetrična kriptografija, također poznata kao kriptografija tajnog ključa, kriptografski je pristup koji se oslanja na jedan dijeljeni tajni ključ i za šifriranje i za dešifriranje podataka. U ovoj metodi, isti ključ koriste i pošiljatelj i primatelj za transformaciju poruke u šifrirani tekst i obrnuto. Sigurnost simetrične kriptografije leži u tajnosti ključa, budući da svatko tko posjeduje ključ može dešifrirati šifriranu poruku. Ova vrsta kriptografije vrlo je učinkovita i prikladna za brzo šifriranje velikih količina podataka. Međutim, zahtijeva siguran kanal za početnu distribuciju tajnog ključa, budući da bi bilo kakvo ugrožavanje ključa moglo dovesti do proboja u šifriranoj komunikaciji. Algoritmi simetrične kriptografije dijele se na protočne šifre (engl. \textit{stream ciphers}) i blok šifre (engl. \textit{block ciphers}). 

Protočne šifre su vrsta simetrične enkripcije koja šifrira podatke bit po bit ili bajt po bajt, što ih čini prikladnima za komunikaciju u stvarnom vremenu i velike tokove podataka. Najčešće protočne šifre uključuju RC4 (\textit{Rivest Cipher 4}) i Salsa20/ChaCha20. Blok šifre, za razliku od protočnih šifri, dijele podatke u blokove fiksne veličine i šifriraju ih pojedinačno. Najčešće blok šifre uključuju DES (\textit{Data encryption standard}), 3DES i AES (\textit{Advanced encryption standard}). Danas blok šifru DES smatramo nesigurnom zbog korištenja tajnog ključa s malim brojem bitova - DES zahtjeva da ključ bude veličine 56 bitova što je po današnjim standardima lako za probiti. 3DES funkcionira poput DES algoritma, samo je primijenjen tri puta za redom, čime je i efektivna veličina ključa dva ili tri puta veća. 

U današnje vrijeme, AES je postao \textit{de facto} standard za sigurnu enkripciju. Povijest AES-a seže u kasne 1990-e kada je Nacionalni institut za standarde i tehnologiju (NIST) pokrenuo javni natječaj za odabir novog algoritma za šifriranje koji će zamijeniti zastarjeli algoritam DES. Javni natječaj pobijedila je šifra Rijndael koju su razvili Joan Daemen i Vincent Rijmen te je 2001. godine usvojena kao AES \cite{aes}. Od tada je AES široko prihvaćen zbog svoje snažne sigurnosti, učinkovitosti i fleksibilnosti. Kao jedna od najpopularnijih opcija za kriptiranje podataka, isprobano je mnogo napada na AES algoritam kako bi se oporavio tajni ključ. Usprkos tome, jedan od najboljih danas poznatih napada treba $2^{126}$ koraka kako bi oporavio tajni ključ, čime je efektivna snaga ključa smanjena sa $128$ bitova na $126$. Iako je AES kao algoritam po današnjim standardima vrlo siguran, njegove implementacije i dalje mogu propuštati informacije korisne u napadima na sporedne kanale.


\section{AES}

AES se temelji na principu dizajna poznatom kao supstitucijsko-permutacijska mreža i učinkovit je i u softveru i u hardveru. Za razliku od svog prethodnika DES-a, AES ne koristi Feistelovu mrežu. Veličina bloka je 128 bita dok veličina ključa može biti 128, 192 ili 256 bita. Većina AES izračuna se radi u određenom konačnom polju - GF($2^8$). Jedan blok se tretira kao pravokutni niz u 4 retka i 4 stupca sa stupcima kao glavnom osi te se naziva stanje. Prikaz stanja prikazan je formulom \ref{aes-state}. Slično se tretira i ključ, posložen u 4 retka te 4, 6, ili 8 stupaca. 

\begin{equation}
    \begin{bmatrix}
        b_0 & b_4 & b_8 & b_{12}\\
        b_1 & b_5 & b_9 & b_{13}\\
        b_2 & b_6 & b_{10} & b_{14}\\
        b_3 & b_7 & b_{11} & b_{15}\\
    \end{bmatrix}
    \label{aes-state}
\end{equation}

Veličina ključa koja se koristi za AES šifru određuje broj rundi: 10 krugova za 128-bitni ključ, 12 krugova za 192-bitni ključ i 14 krugova za 256-bitni ključ. Algoritam enkripcije započinje proširivanjem tajnog ključa (engl. \textit{key expansion}) tj. iz jednog ključa stvara se po jedan za svaku rundu, te dodavanjem prvog potključa poruci (engl. \textit{add round key}). Svaka runda, osim posljednje, se sastoji od 4 koraka: zamijeni znakove (engl. \textit{sub bytes}), posmakni redove (engl. \textit{shift rows}), pomiješaj stupce (engl. \textit{mix columns}) te dodaj odgovarajući potključ rezultatu. Posljednja runda sastoji se od tri koraka: zamijeni znakove, posmakni redove te dodaj posljednji potključ. Dekriptiranje se odvija poput kriptiranja samo u suprotnom smjeru. Postupak je prikazan slikom \ref{aes-rounds}.


\begin{figure}[H]
	\includegraphics[scale=0.55]{images/aes/rounds.png} 
	\centering
	\caption{Postupak kriptiranja i dekriptiranja u AES algoritmu}
	\label{aes-rounds}
\end{figure}

Slika \ref{aes-steps} prikazuje sva četiri koraka koja se izvode nad stanjem. Operacija zamijeni znakove mijenja svaki element stanja s nekim drugim elementom dobivenim pomoću s-kutija (engl. \textit{S-box}). Operacija posmakni redove posmiče redove stanja u lijevo za onoliko mjesta koliki je index tog reda, dok operacija pomiješaj stupce množi stupac stanja s fiksnim polinomom $c(x)$. Um koraku dodavanja ključa, svaki bajt stanja kombinira se s bajtom odgovarajućeg potključa korištenjem operacije xor. 



\begin{figure}[H]%
    \centering
    \subfloat[\centering Dodavanje ključa]{{\includegraphics[width=6cm]{images/aes/add-round-key.png} }}%
    \qquad
    \subfloat[\centering  Zamjena znakova]{{\includegraphics[width=6cm]{images/aes/sub-bytes.png} }}%
    \qquad
    \centering
    \subfloat[\centering Posmicanje redova]{{\includegraphics[width=6cm]{images/aes/shift-rows.png} }}%
    \qquad
    \centering
    \subfloat[\centering Miješanje stupaca]{{\includegraphics[width=6cm]{images/aes/mix-columns.png} }}%
    \qquad
    \caption{Koraci AES algoritma}%
    \label{aes-steps}%
\end{figure}

Cilj napada na AES šifru je otkrivanje tajnog ključa, stoga razmatramo runde čije su informacije lako dostupne. S obzirom na dostupnost javnog teksta ili poruke i šifriranog teksta, najbolji kandidati za napad su prva i zadnja runda. Također, često napadnut korak je korak zamjene znakova, jer izlaz iz s-kutija predstavljaju jedini nelinearni sloj šifre. S-kutija je fiksna tablica pretraživanja koja zamjenjuje svaki bajt ulaza s odgovarajućim bajtom iz prije definirane tablice. U prvoj rundi je ulaz u s-kutiju nadodani čisti tekst s jednim od potključeva, dok je u zadnjoj rundi obrnuta situacija, ulaz u inverznu s-kutiju je nadodani kriptirani tekst na jedan od potključeva. Ovisno o implementaciji, AES može koristiti maskiranje, tehniku koja uključuje dodavanje maske na osjetljive podatke ili određene rezultate kako bi se maskirala njihova stvarna vrijednost, te dodavanje odmaka. U stvarnosti, obje tehnike otežavaju otkrivanje tajnog ključa napadačima, no u teoriji su dostupne i mogu se koristiti kako bi se došlo do tajnog ključa.


U nastavku će se koristiti tri različita skupa podataka s mjerenjima sakupljenim iz napada na sporedne kanale AES šifre. Svaki od skupa podataka napada drugačiji uređaj koji izvodi algoritam kriptiranja ili dekriptiranja, te svaki od skupa podataka napada drugačiju implementaciju AES algoritma.


\section{Skup podataka natječaja DPA v4}


Skup podataka DPA v4 (DPAv4), opisan u \cite{dpa_v4_paper} te javno dostupan na službenim stranicama \cite{dpa_v4}, pruža mjerenja maskirane implementacije AES algoritma s duljinom ključa od $128$ bitova na pametnoj kartici Atmel ATMega-163 na SASEBO-W ploči. To je programska implementacija u kojoj većina curenja informacija proizlazi iz operacije zamijeni znakove, tj. S-kutija, a napadamo prvu rundu. Jedno mjerenje se sastoje od $4000$ značajki prikupljenih oko S-box dijela izvršavanja algoritma.

Skup podataka DPAv4 sastoji se od $80,000$ mjerenja. Zbog nedostatnih računalnih resursa, korišteno je samo prvih $5000$ mjerenja koja su podijeljena u dvije grupe: mjerenja za profiliranje i mjerenja za napad. Mjerenja za profiliranje sastoje se od $4500$ mjerenja, te svako mjerenje ima pripadajući korišten javni tekst i ključ. Mjerenja za napad sastoje se od preostalih $500$ mjerenja, te također svako mjerenje ima pripadajući javni tekst i ključ. Na svim mjerenjima korištena je maska i odmak. Kako napadamo prvu rundu, model curenja informacija definiran je formulom \ref{dpa_model} gdje je $M$ poznata maska, $P_i$ i-ti bajt javnog teksta, a $k$ tajni ključ. Napadamo prvi bajt stoga je $i=0$. Definirani model curenja informacija u klasičnom smislu strojnog učenja znači da će se labele pridružene primjerima dobiti pomoću prije navedene formule - mrežu učimo da predviđa $Y(k)$, a ne sam ključ $k$, te mreži nije dostupan niti čisti tekst $P_i$ niti maska $M$. Nadalje, kako se radi o operacijama nad bajtovima, ukupan broj klasa je broj svih mogućih vrijednosti jednog bajta tj. $256$. Skup za profiliranje podijeljen je na skup podataka za učenje, koji se sastoji od $2250$ mjerenja, te skup podataka za validaciju, koji se sastoji od $2250$ mjerenja.

Pretprocesiranje skupa podataka uključuje primjenu min-max normalizaciju, te z-score normalizacije nad svim mjerenjima, na način da su parametri podešeni samo na skupu za učenje, a primijenjeni nad svim skupovima podataka.


\begin{equation}
    Y(k) = Sbox[P_i \oplus k] \oplus M
    \label{dpa_model}
\end{equation}
 

Za DPAv4, u radu \cite{make_some_noise} pokazano je da postoji regija unutar jednog mjerenja koja sadrži sve relevantne značajke. To znači da se za skup podataka DPAv4 može lako unaprijed odabrati samo najvažnije značajke ili model može naučiti koje su regije bitnije od drugih. Od sva tri navedena skupa podataka, DPAv4 smatra se najlakšim budući da je bez protumjera, a mjerenja imaju malu razinu šuma.


\subsection{Modeli}

Istrenirano je i vrednovano ukupno $3$ različita modela: model potpuno povezane neuronske mreže (MLP), jednostavan model konvolucijske neuronske mreže (SCNN) te kompleksniji model konvolucijske neuronske mreže (CCNN). Hiperparametri za sve modele prikazani su tablicom \ref{aes_dpa_hyp}. Kao algoritam optimizacije korišten je Adam optimizator uz funkciju gubitka unakrsne entropije \ref{cee_loss}.


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccc}
            parametri & MLP & SCNN & CCNN \\ 
            \hline
            broj epoha          & 20    & 50    & 50      \\
            veličina mini grupe & 100   & 50    & 100    \\
            stopa učenja        & 0.001 & 0.001 & 0.0001  \\
            \hline
        \end{tabular}
        \caption{Hiperparametri}
    \label{aes_dpa_hyp}
\end{table}



Model potpuno povezane neuronske mreže (MLP) vrši normalizaciju nad grupom nakon koje slijede $3$ potpuno povezana sloja od $600$ neurona s aktivacijskom funkcijom ReLU, te jedan završni potpuno povezan sloj od $256$ neurona s aktivacijskom funkcijom softmax.


Jednostavan model konvolucijske neuronske mreže (SCNN) sastoji se od jednog konvolucijskog bloka koji sadrži operaciju konvolucije s $2$ jezgre veličine $2$ te nadopunjavanjem nulama praćenom aktivacijskom funkcijom SELU, normalizaciju nad grupom te sažimanje srednjom vrijednošću s veličinom jezgre $2$ i korakom $2$. Nakon konvolucijskog bloka slijede dva potpuno povezana sloja od kojih prvi ima $2$ neurona s aktivacijskom funkcijom SELU, a drugi ima 256 neurona s aktivacijskom funkcijom softmax.


Kompleksniji model konvolucijske neuronske mreže (CCNN) sastoji se od $10$ konvolucijskih blokova, od kojih svaki sadrži operaciju konvolucije s aktivacijskom funkcijom ReLU, bez nadopunjavanja i veličinom jezgre $2$, svaki drugi sadrži normalizaciju nad grupom, te svaki sadrži sažimanje srednjom vrijednošću s veličinom jezgre i korakom $2$. Broj jezgri udvostručuje se u prvih pet koraka od $8$ do $128$ nakon čega slijede dva bloka po $128$ jezgri i tri bloka po $256$ jezgri. Nakon konvolucijskih blokova slijedi $2$ potpuno povezana sloja, od kojih prvi ima $4096$ neurona i aktivacijsku funkciju ReLU, a drugi ima $256$ neurona i aktivacijsku funkciju softmax. Cijela arhitektura detaljnije je prikazana na slici \ref{dpa-cnn-arh}.


\begin{figure}[H]
	\includegraphics[scale=0.47]{images/aes/dpa/dpa-cnn-arh.png} 3
	\centering
	\caption{Arhitektura kompleksnije konvolucijske neuronske mreže}
	\label{dpa-cnn-arh}
\end{figure}


\subsection{Rezultati}

Metrike razmatrane nad ovim modelima uključuju gubitak na skupu za učenje i validacijskom skupu podataka, točnost na validacijskom skupu podataka te entropija pogađanja na validacijskom i testnom skupu podataka. Na ovaj način razmatramo metrike tradicionalne u strojnom učenju poput gubitaka i točnosti, te metriku tradicionalnu u napadima na sporedne kanale tj. entropiju pogađanja. Slike \ref{dpa_loss} prikazuju gubitak svih modela na skupu za učenje te gubitak na validacijskom skupu. Gubitak modela potpuno povezane mreže u mnogo manje epoha postiže niži gubitak od oba modela konvolucijskih mreža, a jednostavnija konvolucijska mreža postiže manji gubitak na oba skupa od kompleksnije. Interesantno je i da gubitak modela kompleksnije konvolucijske mreže na validacijskom skupu zapravo raste. Kod niti jednog modela ne možemo vidjeti krivulje gubitka na skupu za učenje i na validacijskom skupu da formiraju klasičan oblik tih krivulja u strojnom učenju, gdje krivulja gubitka na skupu za učenje pada sve bliže prema nuli, a krivulja na validacijskom skupu pada pa zatim raste. To bi moglo biti do manjka epoha ili nedovoljne ekspresivnosti modela. 

\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/aes/dpa/dpa_train_loss.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/aes/dpa/dpa_val_loss.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu}%
    \label{dpa_loss}%
\end{figure}


Slike \ref{dpa_acc} prikazuju točnost modela na validacijskom skupu podataka, od kojih lijeva uključuje sve modele, a desna slika samo konvolucijske modele. Točnost konvolucijskih modela kreće se uglavnom oko $0.4\%$ i $4\%$, što su u klasičnom smislu strojnog učenja vrlo male vjerojatnosti, dok su u smislu napada na sporedne kanale to očekivane vjerojatnosti. U tom smislu, vjerojatnost koju postiže potpuno povezan model iznenađuje u dobrom smislu jer prelazi čak $40\%$. 

\begin{figure}[H]%
    \centering
    \subfloat[\centering Svi modeli]{{\includegraphics[width=6cm]{images/aes/dpa/dpa_val_acc.png} }}%
    \qquad
    \subfloat[\centering Konvolucijski modeli]{{\includegraphics[width=6cm]{images/aes/dpa/dpa_val_acc_no_mlp.png} }}%
    \qquad
    \caption{Točnost na validacijskom skupu}%
    \label{dpa_acc}%
\end{figure}


Konačno, za svaki model na slikama \ref{dpa_ge} prikazana je entropija pogađanja na validacijskom skupu podataka, te skupu podataka za testiranje. Iz prve dvije slike možemo vidjeti da modeli potpuno povezane mreže, te jednostavne konvolucijske mreže postižu savršenu entropiju pogađanja s vrlo malim brojem mjerenja. Treća slika prikazuje da i kompleksniji konvolucijski model dostiže savršenu entropiju pogađanja, ali s više potrebnih mjerenja. To znači da napadač koji koristi predložene modele može prosječno iz prvog pokušaja pogoditi tajni ključ. Potreban broj mjerenja i entropija pogađanja svih modela prikazani su u tablici \ref{dpa_ge_table}. 

\begin{figure}[H]%
    \centering
    \subfloat[\centering Potpuno povezan model (MLP)]{{\includegraphics[width=6cm]{images/aes/dpa/dpa_mlp_ge.png} }}%
    \qquad
    \subfloat[\centering Jednostavan konvolucijski model (SCNN)]{{\includegraphics[width=6cm]{images/aes/dpa/dpa-scnn-ge.png} }}%
    \qquad
    \subfloat[\centering Kompleksniji konvolucijski model (CCNN)]{{\includegraphics[width=6cm]{images/aes/dpa/dpa-ccnn-ge.png} }}%
    \qquad
    \caption{Entropija pogađanja (GE) u ovisnosti o broju mjerenja na validacijskom skupu podataka i skupu podataka za testiranje}%
    \label{dpa_ge}%
\end{figure}


\begin{table}
    \centering
        \begin{tabular}{c|ccc}
            skup podataka za validaciju &  MLP & SCNN & CCNN \\ 
            \hline
            entropija pogađanja & 1.0 & 1.0 & 1.0 \\
            potreban broj mjerenja  & 1 & 3 & 172 \\
            \hline
        \end{tabular}
        \\
        \vspace{1cm}
        \begin{tabular}{c|ccc}
            skup podataka za testiranje &  MLP & SCNN & CCNN \\ 
            \hline
            entropija pogađanja & 1.0 & 1.0 & 1.0\\
            potreban broj mjerenja & 1 & 3 & 136\\
            \hline
        \end{tabular}
        
        \caption{Entropija pogađanja te potreban broj mjerenja za dostizanje savršene entropije pogađanja, ako je entropija pogađanja savršena, na validacijskom i testnom skupu podataka}
    \label{dpa_ge_table}
\end{table}


\section{ASCAD}

Skup podataka ASCAD, opisan u \cite{ascad_paper} pruža mjerenja elektromagnetskog zračenja programske implementacije maskiranog AES algoritma s veličinom ključa od $128$ bita sakupljenih na 8-bitnom AVR mikrokontroleru ATmega8515. Poput DPAv4 skupa podataka, većina curenja proizlazi iz koraka zamjene znakova uz pomoć S-kutija te napadamo prvu rundu. Jedno mjerenje sastoji se od $700$ relevantnih uzoraka koji odgovaraju maskiranom izlazu S-kutije za treći bajt.

Skup podataka ASCAD sastoji se od $60000$ mjerenja organiziranih nalik na MNIST bazu podataka, gdje je $50000$ mjerenja korišteno za profiliranje, a $10000$ za testiranje. Oba skupa također pružaju javne tekstove i ključeve korištene u svakom mjerenju. Model curenja informacija definiran je formulom \ref{ascad_model}, gdje je $P_i$ i-ti bajt javnog teksta, a $k$ ključ. Kako su mjerenja odrađena u blizini obrade drugog bajta odabiremo $i=2$. Ukupan broj klasa je $256$. Skup mjerenja za profiliranje nadalje je podijeljen na skup za učenje i skup za validaciju od kojih svaki ima a po $25000$ mjerenja. 

Pretprocesiranje skupa podataka uključuje primjenu min-max normalizaciju, te z-score normalizacije nad svim mjerenjima, tako da su parametri podešeni samo na skupu za učenje, a primijenjeni nad svim skupovima podataka.


\begin{equation}
    Y(k) = Sbox[P_i \oplus k]
    \label{ascad_model}
\end{equation}
 

Rad \cite{make_some_noise} pokazao je da ASCAD skup podataka nema određenu regiju unutar jednog mjerenja koja sadrži sve relevantne značajke, ali postoje regije koje sadrže bitnije informacije od drugih regija. To znači da je ASCAD skup podataka teži od DPAv4 skupa podataka. 


\subsection{Modeli}

Poput DPAv4 skupa podataka, istrenirano je i vrednovano ukupno $3$ različita modela: model potpuno povezane neuronske mreže (MLP), jednostavan model konvolucijske neuronske mreže (SCNN) te kompleksniji model konvolucijske neuronske mreže (CCNN). Hiperparametri za sve modele prikazani su tablicom \ref{aes_ascad_hyp}. Kao algoritam optimizacije korišten je Adam optimizator uz funkciju gubitka unakrsne entropije \ref{cee_loss}.


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccc}
            parametri & MLP & SCNN & CCNN \\ 
            \hline
            broj epoha          & 50    & 50    & 50      \\
            veličina mini grupe & 100   & 100    & 256    \\
            stopa učenja        & 0.0001 & 0.001 & 0.0001  \\
            \hline
        \end{tabular}
        \caption{Hiperparametri}
    \label{aes_ascad_hyp}
\end{table}



Model potpuno povezane neuronske mreže (MLP) vrši normalizaciju nad grupom nakon koje slijedi $5$ potpuno povezanih slojeva od $600$ neurona s aktivacijskom funkcijom ReLU, te jedan završni potpuno povezan sloj od $256$ neurona s aktivacijskom funkcijom softmax.


Jednostavan model konvolucijske neuronske mreže (SCNN) sastoji se od jednog konvolucijskog bloka koji sadrži operaciju konvolucije s $4$ jezgre veličine $2$ te nadopunjavanjem nulama praćenom aktivacijskom funkcijom SELU, normalizaciju nad grupom te sažimanje srednjom vrijednošću s veličinom jezgre $2$ i korakom $2$. Nakon konvolucijskog bloka slijede dva potpuno povezana sloja od $10$ neurona s aktivacijskom funkcijom SELU te završni potpuno povezan sloj od $256$ neurona s aktivacijskom funkcijom softmax.


Kompleksniji model konvolucijske neuronske mreže (CCNN) sastoji se od $8$ konvolucijskih blokova, od kojih svaki sadrži operaciju konvolucije s aktivacijskom funkcijom ReLU, bez nadopunjavanja i veličinom jezgre $2$, svaki drugi sadrži normalizaciju nad grupom, te svaki sadrži sažimanje maksimumom s veličinom jezgre i korakom $2$. Broj jezgri udvostručuje se u prvih pet koraka od $8$ do $128$ nakon čega slijedi jedan blok od $128$ jezgri i dva bloka po $256$ jezgri. Nakon konvolucijskih blokova slijedi $2$ potpuno povezana sloja s $256$ neurona odvojena Dropout slojem, od kojih prvi ima aktivacijsku funkciju ReLU, a drugi softmax.  Cijela arhitektura detaljnije je prikazana na slici \ref{ascad-cnn-arh}.


\begin{figure}[H]
	\includegraphics[scale=0.47]{images/aes/ascad/ascad-cnn-arh.png}
	\centering
	\caption{Arhitektura kompleksnije konvolucijske neuronske mreže}
	\label{ascad-cnn-arh}
\end{figure}

\subsection{Rezultati}

Razmatrane metrike kao i do sada uključuju gubitak na skupu za učenje i validacijskom skupu podataka, točnost na validacijskom skupu podataka te entropiju pogađanja na validacijskom i testnom skupu podataka. Slike \ref{ascad_loss} prikazuju gubitak svih modela na skupu za učenje te gubitak na validacijskom skupu. Gubitak modela potpuno povezane mreže na skupu za učenje najstrmije pada, dok na skupu za validaciju najstrmije raste. Gubitak oba konvolucijska modela uglavnom se ne mijenja niti na skupu za učenje niti na skupu za validaciju.

\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/aes/ascad/ascad_loss.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/aes/ascad/ascad_val_loss.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu}%
    \label{ascad_loss}%
\end{figure}


Slika \ref{ascad_acc} prikazuju točnost modela na validacijskom skupu podataka. Točnost svih modela kreće se uglavnom od $0.2\%$ do $0.9\%$, a najveći rast tijekom epoha pokazao je model jednostavne konvolucijske mreže (SCNN). Kao i prethodno, uzimajući u obzir kontekst napada na sporedne kanale to su očekivane vrijednosti vjerojatnosti. 


\begin{figure}[H]
	\includegraphics[scale=0.55]{images/aes/ascad/ascad_val_acc.png}
	\centering
	\caption{Točnost na validacijskom skupu}
	\label{ascad_acc}
\end{figure}



Konačno, za svaki model na slikama \ref{ascad_ge} prikazana je entropija pogađanja na validacijskom skupu podataka, te skupu podataka za testiranje. Iz prve dvije slike možemo vidjeti da modeli potpuno povezane mreže, te jednostavne konvolucijske mreže postižu savršenu entropiju pogađanja uz to da je jednostavnoj konvolucijskoj mreži potrebno manje mjerenja od potpuno povezane mreže. Treća slika prikazuje da kompleksniji konvolucijski model ne dostiže savršenu entropiju pogađanja, ali bi mogao uz više epoha kod treniranja. Potreban broj mjerenja i entropija pogađanja svih modela prikazani su u tablici \ref{ascad_ge_table}.Napadač koji koristi MLP model u prosjeku treba $1$ pokušaj i $1460$ mjerenja da pogodi ključ. Ako koristi SCNN model u prosjeku će također iz prvog pokušaja pogoditi ključ, ali mu je potrebno samo $142$ mjerenja. Napadaču koji koristi CCNN model potrebno je oko $50$ pokušaja da pogodi ključ.


\begin{figure}[H]%
    \centering
    \subfloat[\centering Potpuno povezan model (MLP)]{{\includegraphics[width=6cm]{images/aes/ascad/ascad_mlp_ge.png} }}%
    \qquad
    \subfloat[\centering Jednostavan konvolucijski model (SCNN)]{{\includegraphics[width=6cm]{images/aes/ascad/ascad_scnn_ge.png} }}%
    \qquad
    \subfloat[\centering Kompleksniji konvolucijski model (CCNN)]{{\includegraphics[width=6cm]{images/aes/ascad/ascad_ccnn_ge.png} }}%
    \qquad
    \caption{Entropija pogađanja (GE) u ovisnosti o broju mjerenja na validacijskom skupu podataka i skupu podataka za testiranje}%
    \label{ascad_ge}%
\end{figure}



\begin{table}
    \centering
        \begin{tabular}{c|ccc}
            skup podataka za validaciju &  MLP & SCNN & CCNN \\ 
            \hline
            entropija pogađanja & 1.0 & 1.0 & 144.0 \\
            potreban broj mjerenja  & 1190 & 126 & - \\
            \hline
        \end{tabular}
        \\
        \vspace{1cm}
        \begin{tabular}{c|ccc}
            skup podataka za testiranje &  MLP & SCNN & CCNN \\ 
            \hline
            entropija pogađanja & 1.0 & 1.0 & 50.0\\
            potreban broj mjerenja & 1460 & 142 & -\\
            \hline
        \end{tabular}
        
        \caption{Entropija pogađanja te potreban broj mjerenja za dostizanje savršene entropije pogađanja, ako je entropija pogađanja savršena, na validacijskom i testnom skupu podataka}
    \label{ascad_ge_table}
\end{table}


\section{AES\_HD}

Skup podataka AES\_HD, opisan u \cite{AES_HD}, pruža mjerenja elektromagnetskog zračenja nezaštićene hardverske implementacije AES algoritma s veličinom ključa od $128$ bita prikupljena pomoću visoko osjetljive sonde. Jezgra algoritma napisana je u VHDL-u na arhitekturi baziranoj na rundama, kojoj je potrebno 11 ciklusa takta za svaku enkripciju. Jezgra AES-128 omotana je UART modulom kako bi se omogućila vanjska komunikacija. Ukupna površina otiska dizajna sadrži 1 850 LUT i 742 bistabila. Dizajn je implementiran na Xilinx Virtex-5 FPGA evaluacijskoj ploči SASEBO GII. Jedno mjerenje sastoji se od $700$ relevantnih uzoraka.


Skup podataka AES\_HD sastoji se od $50000$ mjerenja organiziranih u dvije grupe mjerenja - mjerenja za profiliranje, koja sadrže $45000$ mjerenja, te mjerenja za napad, koja sadrže $5000$ mjerenja. Skup podataka pruža javne tekstove i ključeve korištene u svakom mjerenju. Napada se posljednja runda zbog čega je prikladan i često korišten model curenja informacija nezaštićene sklopovske implementacije upisivanje u registar u posljednjoj rund. Model definiramo formulom \ref{aes_hd_model}, gdje su $C_i$ i $C_j$ dva bajta kriptiranog teksta, a relacija između $i$ i $j$ dana je inverzom koraka posmakni redove u AES algoritmu. Napada se dvanaesti bajt, što rezultira s $i=11$ i $j=7$. Ukupan broj klasa je $256$. Skup mjerenja za profiliranje nadalje je podijeljen na skup za učenje i skup za validaciju od kojih svaki ima a po $22500$ mjerenja. 


Pretprocesiranje skupa podataka uključuje primjenu min-max normalizaciju, te z-score normalizacije nad svim mjerenjima, tako da su parametri podešeni samo na skupu za učenje, a primijenjeni nad svim skupovima podataka.


\begin{equation}
    Y(k) = Sbox^{-1}[C_i \oplus k] \oplus C_j
    \label{aes_hd_model}
\end{equation}


 
Mjerenja AES\_HD skup podataka dosta su šumna te nema određenu regiju unutar jednog mjerenja koja sadrži sve relevantne značajke što je pokazano u radu \cite{make_some_noise}. Od sva tri navedena skupa podataka, AES\_HD smatra se najtežim.


\subsection{Modeli}


Poput prethodna dva skupa podataka, istrenirano je i vrednovano ukupno $3$ različita modela: model potpuno povezane neuronske mreže (MLP), jednostavan model konvolucijske neuronske mreže (SCNN) te kompleksniji model konvolucijske neuronske mreže (CCNN). Hiperparametri za sve modele prikazani su tablicom \ref{aes_hd_hyp}. Kao algoritam optimizacije korišten je Adam optimizator uz funkciju gubitka unakrsne entropije \ref{cee_loss}.


\begin{table}[H]
    \centering
        \begin{tabular}{c|ccc}
            parametri & MLP & SCNN & CCNN \\ 
            \hline
            broj epoha          & 50    & 50    & 50      \\
            veličina mini grupe & 100   & 256    & 256    \\
            stopa učenja        & 0.0001 & 0.001 & 0.0001  \\
            \hline
        \end{tabular}
        \caption{Hiperparametri}
    \label{aes_hd_hyp}
\end{table}



Model potpuno povezane neuronske mreže (MLP) vrši normalizaciju nad grupom nakon koje slijedi $5$ potpuno povezanih slojeva od $600$ neurona s aktivacijskom funkcijom ReLU, te jedan završni potpuno povezan sloj od $256$ neurona s aktivacijskom funkcijom softmax.


Jednostavan model konvolucijske neuronske mreže (SCNN) sastoji se od jednog konvolucijskog bloka koji sadrži operaciju konvolucije s $2$ jezgre veličine $2$ te nadopunjavanjem nulama praćenom aktivacijskom funkcijom SELU, normalizaciju nad grupom te sažimanje srednjom vrijednošću s veličinom jezgre $2$ i korakom $2$. Nakon konvolucijskog bloka slijedi potpuno povezan sloj od $2$ neurona s aktivacijskom funkcijom SELU te završni potpuno povezan sloj od $256$ neurona s aktivacijskom funkcijom softmax.


Kompleksniji model konvolucijske neuronske mreže (CCNN) sastoji se od $8$ konvolucijskih blokova, od kojih svaki sadrži operaciju konvolucije s aktivacijskom funkcijom ReLU, bez nadopunjavanja i veličinom jezgre $2$, svaki drugi sadrži normalizaciju nad grupom, te svaki sadrži sažimanje maksimumom s veličinom jezgre i korakom $2$. Broj jezgri udvostručuje se u prvih pet koraka od $8$ do $128$ nakon čega slijedi jedan blok od $128$ jezgri i dva bloka po $256$ jezgri. Nakon konvolucijskih blokova slijedi potpuno povezan sloj s $512$ neurona i aktivacijskom funkcijom ReLU, Dropout sloj, te posljednji sloj od $256$ neurona s aktivacijskom funkcijom softmax.  Cijela arhitektura detaljnije je prikazana na slici \ref{aes_hd-cnn-arh}.


\begin{figure}[H]
	\includegraphics[scale=0.47]{images/aes/aes_hd/aes_hd-cnn-arh.png}
	\centering
	\caption{Arhitektura kompleksnije konvolucijske neuronske mreže}
	\label{aes_hd-cnn-arh}
\end{figure}


\subsection{Rezultati}


Razmatrane metrike kao i do sada uključuju gubitak na skupu za učenje i validacijskom skupu podataka, točnost na validacijskom skupu podataka te entropiju pogađanja na validacijskom i testnom skupu podataka. Slike \ref{aes_hd_loss} prikazuju gubitak svih modela na skupu za učenje te gubitak na validacijskom skupu. Prvo što možemo primijetiti je da opet model potpuno povezane mreže ima velik pad gubitka na skupu za učenje i simultani rast na skupu za validaciju. Ovaj put ima promjena kod konvolucijskih modela, a to je da kompleksni konvolucijski model ima nižu grešku od jednostavnog konvolucijskog modela. 

\begin{figure}[H]%
    \centering
    \subfloat[\centering Skup za učenje]{{\includegraphics[width=6cm]{images/aes/aes_hd/aes_hd_loss.png} }}%
    \qquad
    \subfloat[\centering  Validacijski skup]{{\includegraphics[width=6cm]{images/aes/aes_hd/aes_hd_valid_loss.png} }}%
    \qquad
    \caption{Gubitak na skupu za učenje i validacijskom skupu}%
    \label{aes_hd_loss}%
\end{figure}


Slika \ref{aes_hd_acc} prikazuju točnost modela na validacijskom skupu podataka. Točnost svih modela kreće se uglavnom od $0.2\%$ do $0.7\%$, te ne možemo izdvojiti ikoji model koji se ističe iznad drugih modela.  


\begin{figure}[H]
	\includegraphics[scale=0.55]{images/aes/aes_hd/aes_hd_valid_acc.png}
	\centering
	\caption{Točnost na validacijskom skupu}
	\label{aes_hd_acc}
\end{figure}



Konačno, za svaki model na slikama \ref{aes_hd_ge} prikazana je entropija pogađanja na validacijskom skupu podataka, te skupu podataka za testiranje. Prva slika prikazuje model potpuno povezane mreže koja ima konzistentno smanjenje entropije pogađanja koja pada s rastom broja mjerenja, ali nikad ne dostiže optimalnu razinu gdje je entropija pogađanja jednaka $1$. Druga slika prikazuje entropiju pogađanja za jednostavni konvolucijski model te možemo primijetiti da entropija pogađanja na skupu za napad pada, dok na skupu za profiliranje stagnira na dosta visokoj razini od $140$. Posljednja slika prikazuje entropiju pogađanja za kompleksniji konvolucijski model te se on ponaša gotovo skroz suprotno od jednostavnog konvolucijskog modela. Nikoji model ne dostiže savršenu entropiju pogađanja, ali s obzirom na težinu skupa podataka to ne iznenađuje. Najbolje rezultate na skupu za napad pokazuje jednostavna konvolucijska mreža s entropijom pogađanja $31$. Potreban broj mjerenja i entropija pogađanja svih modela prikazani su u tablici \ref{aes_hd_ge_table}. 


\begin{figure}[H]%
    \centering
    \subfloat[\centering Potpuno povezan model (MLP)]{{\includegraphics[width=6cm]{images/aes/aes_hd/aes_hd_mlp_ge.png} }}%
    \qquad
    \subfloat[\centering Jednostavan konvolucijski model (SCNN)]{{\includegraphics[width=6cm]{images/aes/aes_hd/aes_hd_scnn_ge.png} }}%
    \qquad
    \subfloat[\centering Kompleksniji konvolucijski model (CCNN)]{{\includegraphics[width=6cm]{images/aes/aes_hd/aes_hd_ccnn_ge.png} }}%
    \qquad
    \caption{Entropija pogađanja (GE) u ovisnosti o broju mjerenja na validacijskom skupu podataka i skupu podataka za testiranje}%
    \label{aes_hd_ge}%
\end{figure}



\begin{table}
    \centering
        \begin{tabular}{c|ccc}
            skup podataka za validaciju &  MLP & SCNN & CCNN \\ 
            \hline
            entropija pogađanja & 47.0 & 136.0 & 73.0 \\
            potreban broj mjerenja  & - & - & - \\
            \hline
        \end{tabular}
        \\
        \vspace{1cm}
        \begin{tabular}{c|ccc}
            skup podataka za testiranje &  MLP & SCNN & CCNN \\ 
            \hline
            entropija pogađanja & 75.0 & 31.0 & 143.0\\
            potreban broj mjerenja & - & - & -\\
            \hline
        \end{tabular}
        
        \caption{Entropija pogađanja te potreban broj mjerenja za dostizanje savršene entropije pogađanja, ako je entropija pogađanja savršena, na validacijskom i testnom skupu podataka}
    \label{aes_hd_ge_table}
\end{table}
