\chapter{Presjek strojnog učenja i napada na sporedne kanale}


Presjek strojnog učenja i napada sporednih kanala leži u primjeni tehnika strojnog učenja za iskorištavanje informacija pribavljenih iz sporednih kanala. Napadi na sporedne kanale iskorištavaju nenamjerno curenje informacija iz fizičkih implementacija kriptografskih algoritama, poput potrošnje energije, vremenskih varijacija ili elektromagnetskog zračenja. Algoritmi strojnog učenja mogu se uvježbati za analizu i prepoznavanje uzoraka unutar podataka sporednog kanala, omogućujući točnije i učinkovitije izdvajanje osjetljivih informacija. Iskorištavanjem snage strojnog učenja, napadači mogu poboljšati svoju sposobnost izvođenja uspješnih napada na sporedne kanale, potencijalno ugrožavajući sigurnost kriptografskih sustava. U isto vrijeme, razvoj protumjera temeljenih na strojnom učenju postaje imperativ kako bi se ublažili rizici povezani s napadima na sporedne kanale i osigurala robusnost kriptografskih implementacija.


\section{Strojno učenje}


Strojno učenje, kao potpodručje umjetne inteligencije, omogućuje sustavima da uče iz podataka i donose inteligentna predviđanja ili odluke bez eksplicitnog programiranja. Modeli strojnog učenja koriste se za rješavanje različitih vrsta problema poput klasifikacije, regresije ili generiranja podataka. Klasifikacija označava da model treba odrediti neku diskretnu/nebrojčanu klasu u koju dani primjer pripada, dok kod regresije model za dani primjer daje neku kontinuiranu/brojčanu vrijednost na izlazu. Postoje tri glavna pristupa strojnom učenju koja se razlikuju u dostupnim informacijama pri učenju: nadzirano, nenadzirano i podržano strojno učenje. Nadzirano strojno učenje zahtjeva da se podaci sastoje od ulaza i njihovih željenih izlaza, a cilj je naučiti opće pravilo koje mapira ulaze u izlaze. Nenadzirano učenje koristi se u slučajevima kada nam željeni izlazi nisu poznati, a cilj je grupirati podatke, procijeniti njihovu gustoću ili im smanjiti dimenzionalnost. Podržano učenje je učenje optimalne strategije na temelju pokušaja s odgođenom nagradom.


Generalno, u strojnom učenju imamo model koji uči nad jednim dijelom podataka kojeg nazivamo skupom za učenje, te se vrednuje nad drugim dijelom podataka kojeg nazivamo skup za testiranje.  Često se koristi i skup za validaciju, skup nad kojim tijekom treniranja provjeravamo koliko trenutni model dobro generalizira. Glavni cilj korištenja skupa za validaciju je sprječavanje prenaučenosti, fenomena u strojnom učenju u kojem se model previše prilagodio skupu za učenje pa je naučio i šum zbog čega loše generalizira, te podnaučenosti. Treniranje podrazumijeva izračun funkcije pogreške, tj. procjena koliko je trenutni model dobar, te na temelju funkcije pogreške mijenjanje modela kako bi se ona smanjila. Validacija modela podrazumijeva testiranje modela na još neviđenim podacima te izračun metrika bitnih ovisno o skupu podataka. Jedna od najbitnijih metrika u strojnom učenju je točnost - broj točno klasificiranih podataka od ukupnog broja podataka.


Neki modeli strojnog učenja korišteni u ovom radu su konvolucijske neuronske mreže (engl. \textit{convolutional neural networks} - CNN), potpuno povezane neuronske mreže (engl. \textit{multi layer perceptron} - MLP), stroj potpornih vektora (engl. \textit{support vector machines} - SVM), te mreže s dugom kratkoročnom memorijom (engl. \textit{long short term memory} - LSTM). 


Konvolucijske neuronske mreže su modeli posebno dizajnirani za analizu vizualnih podataka korištenjem hijerarhijskih obrazaca i lokalnih receptivnih polja kroz konvolucijske slojeve. Sastoje se od konvolucijskih slojeva, slojeva sažimanja te potpuno povezanih slojeva. Osim u analizi slike pokazale su se kao dobri modeli u analizi zvukova i signala što je motivacija za njihovo korištenje u napadima na sporedne kanale. 


Potpuno povezana neuronska mreža, još nazivana i višeslojni perceptron, je umjetna neuronska mreža sastavljena od više slojeva međusobno povezanih neurona, gdje svaki neuron izvodi otežani zbroj svojih ulaza i primjenjuje nelinearnu aktivacijsku funkciju za generiranje izlaza.


Stroj potpornih vektora je model strojnog učenja koji klasificira podatke pronalaženjem optimalne hiperravnine koja maksimalno razdvaja različite klase u visokodimenzionalnom prostoru značajki. Važan pojam vezan uz stroj potpornih vektora su jezgre - funkcije koje se koriste za transformaciju ulaznih podataka u višedimenzionalni prostor. Često korištene SVM jezgre uključuju linearne, polinomne i  radijalne bazične funkcije (RBF). 


Mreža s dugom kratkoročnom memorijom je arhitektura povratne neuronske mreže (engl. \textit{recurrent neural network} - RNN) koja može učinkovito naučiti dugoročne ovisnosti u sekvencijalnim podacima korištenjem memorijskih ćelija i mehanizama usmjeravanja za selektivno zadržavanje i zaboravljanje informacija tijekom vremena. 


Prvi korak primjene strojnog učenja je analiza i pretprocesiranje podataka. Pretprocesiranje podataka odnosi se na niz koraka i tehnika primijenjenih na neobrađene podatke poput čišćenja, normalizacije, i skaliranja značajki. Dvije bitne vrste skaliranja su min-max skaliranje, prikazano u formuli \ref{min-max}, i z-score normalizacija, prikazana u formuli \ref{z-score}.


\begin{equation}
    x_{scaled} = \frac{x - min(x)}{max(x) - min(x)}
    \label{min-max}
\end{equation}

\begin{equation}
    x_{scaled} = \frac{x - \mu(x)}{\sigma(x)}
    \label{z-score}
\end{equation}



\section{Napadi na sporedne kanale}

Napadi na sporedne kanale iskorištavaju informacije koje su procurile iz fizičkih implementacija kriptografskih algoritama. Budući da to curenje (npr. potrošnja energije ili elektromagnetsko zračenje) ovisi o nekom malom dijelu interno korištenog tajnog ključa, napadač može izvesti učinkovit napad ekstrakcije ključa kako bi otkrio osjetljive podatke. Među napadima na sporedne kanale (engl. \textit{side channel attacks} - SCA) mogu se razlikovati dvije klase: skup takozvanih profilirajućih SCA napada te skup SCA napada bez profila. Skup profilirajućih SCA napada su zapravo najjači napadi budući da pretpostavljaju da protivnik može prethodno upotrijebiti identičnu kopiju uređaja koju napada za precizno podešavanje svih parametra napada. Profilirajući SCA napadi sastoje se od dva koraka. U prvom koraku napadač nabavlja kopiju ciljanog uređaja i koristi je za karakterizaciju fizičkog curenja podataka, a u drugom izvodi napad ekstrakcije ključa na ciljni uređaj. Ova kategorija napada uključuje napade predlošcima (engl. \textit{template attacks}) \cite{TA} i linearnu regresijsku analizu. Skup SCA napada bez profila odgovaraju puno slabijem napadaču koji ima pristup samo fizičkom curenju podataka zabilježenom na ciljnom uređaju. Kako bi ekstrahirao korišteni tajni ključ, izvodi neke statističke analize nad mjerenjima. Ovaj skup napada bez profiliranja uključuje, diferencijalnu analizu snage (engl. \textit{differential power analysis} - DPA), analizu elektromagnetske emisije (engl. \textit{electromagnetic attack}) i napade mjerenjem vremena (engl. \textit{timing attacks})


Napad praćenjem elektromagnetske energije temelji se na curenju elektromagnetskog zračenja koje se može koristiti za otkrivanje kriptografskih ključeva ili u ne-kriptografskim napadima poput TEMPEST napada \cite{tempest}, gdje napadač špijunira električni uređaj žrtve pomoću opreme koja snima elektromagnetske transmisije. Napad mjerenjem vremena temelje se na proučavanju potrebnog vremena za određene operacije. Diferencijalna analiza snage koristi razinu potrošnje električne energije na temelju koje ekstrapolira tajni ključ. Slika \ref{rsa} prikazuje napad jednostavne analize snage te kako se pažljivim monitoriranjem potrošnje električne energije u izvedbi  kriptografskog algoritma RSA može iščitati cijeli privatni ključ.


\begin{figure}[H]
	\includegraphics[scale=0.7]{images/rsa.png} 
	\centering
	\caption{Jednostavan analiza snage na algoritmu RSA preuzeta iz \cite{sparsa}}
	\label{rsa}
\end{figure}


Jedna od glavnih metrika koja se koristi u napadima na sporedne kanale je entropija pogađanja (engl. \textit{guessing entropy}). Ona navodi prosječan broj kandidata ključeva koje napadač treba testirati kako bi otkrio tajni ključ nakon provođenja analize sporednih kanala te je opisana u \cite{guessing_entropy}. S obzirom na količinu mjerenja u fazi napada, napad daje vektor vjerojatnosti svih ključeva $g= [g_1,g_2, \dots ,g_{|K|}]$ gdje je $g_1$ najvjerojatniji, a $g_{|K|}$ najmanje vjerojatan kandidat za točan ključ. Pretpostavljena entropija je prosječna pozicija točnog ključa $k$ u $g$ tijekom više eksperimenata. Nadalje, za izračunavanje vektora vjerojatnosti svih ključeva $g$ uz više uzoraka koristimo se načelom logaritma vjerojatnosti prikazanim formulom \ref{gi}.


\begin{equation}
    g_i = \sum_{j=0} ^{|D|} log(p_{ij})
    \label{gi}
\end{equation}


\section{Strojno učenje u napadima na sporedne kanale}


U slučaju primjene strojnog učenja u napadima na sporedne kanale govorit ćemo o profilirajućim SCA napadima te o klasifikaciji gdje su u pravilu klase bajtovi tajnog ključa. Ponekad će klase biti definirane modelom curenja informacija $Y(\cdot)$ koji je bijektivna funkcija koja daje vezu između ključa i izlaza. U tim slučajevima, za svaki pojedini ključ $k$ izračunat ćemo $Y(k)$ te pomoću njih računati odabrane metrike koje su u našem slučaju točnost i entropija pogađanja.


